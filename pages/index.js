import React from 'react';
import {Provider} from 'react-redux';
import Tokens from '../components/tokens';
import store from '../redux/store';


export default function Home(){

    return (
        <Provider store={store}>
            <Tokens/>
        </Provider>
    )
}