export const UPDATE_BTC_PRICE = "UPDATE_BTC_PRICE";

export const updateBTCPrice = (btcPrice) => ({
    value: btcPrice,
    type: 'UPDATE_BTC_PRICE'
});