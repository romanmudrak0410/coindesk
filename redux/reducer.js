import {combineReducers} from 'redux';
import {UPDATE_BTC_PRICE} from './actions';

const initialState = {
    btc: 0,
    ethPrice: 0
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_BTC_PRICE:
            return {...state, btc: action.value};
        default:
            return {...state}; 
    }
};

const rootReducer = combineReducers({
    tokens: reducer
});

export default rootReducer;