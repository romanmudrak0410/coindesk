import React, { useEffect } from 'react';

function Tokens({tokens, updateBTCPrice}){

     const getTokenPrice = () => {
        fetch('https://api.coindesk.com/v1/bpi/currentprice.json')
        .then((res) => res.json())
        .then((data) => {
            updateBTCPrice(data)
        }) 
     }


    useEffect(() => {
      getTokenPrice();
      setInterval(getTokenPrice, 15000);
    }, [])


    if (!tokens.btc) return <div>Loading...</div>

    
    return (
        <div>
            <p>{tokens.btc.chartName}: ${tokens.btc.bpi.USD.rate_float}</p>
            <p>BTC price update every 15 seconds</p>
        </div>
    )
}

export default Tokens;
