import presentational from './presentational';
import {connect} from 'react-redux';
import { updateBTCPrice } from '../../redux/actions';

const mapStateToProps = state => ({
    tokens: state.tokens
});

const mapDispatchToProps = {
    updateBTCPrice: updateBTCPrice,
};

export default connect(mapStateToProps, mapDispatchToProps)(presentational);
